import paho.mqtt.client as mqtt
import configparser as cfg
import json

class mqtt_handler:

    def __init__(self, host, port, id, topic, callback):
        print(callback)
        #Initalisation
        self.host = host
        self.port = port
        self.id = id
        self.topic = topic

        self.client = mqtt.Client()

        #Initialisation des variable liée a la reception des message depuis une file
        self.client.on_connect = self.on_connect
        self.client.on_message = callback
        self.client.on_publish = self.on_publish

        self.client.connect(self.host, self.port)

        #Abonnement au topic de la file
        self.client.subscribe(self.topic)


#Getter
    def get_client(self):
        return self.client
    
    def subscribe_topic(self):
        self.client.subscribe(self.topic)

    def publish(self, msg):
        print("Envoie du message : ", msg, " dans : ", self.topic)
        self.client.publish(self.topic, msg)

#Conneciton a la file mqtt
    def on_connect(self, client, userdata, flags, rc):
        if(rc == 0):
            print("Connecté a la file !")
        else:
            print("Erreur lors de la connextion à la file")

    def on_publish(self, client, userdata, result):
        print("pub")
