from etat import Etat

class traficlight:
    def __init__(self, posx, posy, id, dir):
        self.posx = int(posx)
        self.posy = int(posy)
        self.dir = int(dir)
        self.id = int(id)
        self.etat = Etat.GREEN

    #Getter
    def get_pos(self):
        return self.posx, self.posy
    
    def get_id(self):
        return self.id
    
    def get_etat(self):
        return self.etat

    def get_dir(self):
        return self.dir
    
    #Update de l'etat du feu
    def update(self, new_etat):
        #On verifie que le nouvel etat est bien une instance de l'enum Etat
        if(new_etat is Etat.GREEN or new_etat is Etat.RED):
            self.etat = new_etat
    
    def print_light(self):
        print("pos : ", self.posx, self.posy, "\nid : ", self.id, "\netat : ", self.etat, "\ndir : ", self.dir)
