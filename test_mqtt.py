import paho.mqtt.client as mqtt

def on_message(client, userdata, message):
    print(message.payload.decode('utf-8'))

def on_connect(client, userdata, flags, rc):
    if(rc == 0):
        print("Connecté a la file !")
    else:
        print("Erreur lors de la connextion à la file")

client = mqtt.Client()
host = '194.57.103.203'
port = 1883

client.on_message = on_message
client.on_connect = on_connect

client.connect(host, port)
print(client.subscribe('positions'))

client.loop_forever()

print("test")