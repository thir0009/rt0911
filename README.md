## Exécution du projet
Clone et pull le projet.
Pour lancer l'exécution de l'application entrer la commande **python main.py**

## Configuration
Vous pouvez modifier le fichier **config.ini** au besoin, ce fichier contient les informations de connexions relatives a la file mqtt et aux noms des topic.