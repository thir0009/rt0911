import map, json, car, time, os, sys
from mqtt_handler import *
from view import *
from threading import *

test = False

if(sys.argv == 2):
    if(str(sys.argv[1]) == "test"):
        test = True

#Variable global pour la mise à jour des message reçu depuis la file
json_init = None
json_cars = None
json_traficlight = None
json_top = None


#Callback lors de la reception de message depuis un topic
def on_message(client, userdata, message):
    global json_init
    global json_cars
    global json_traficlight
    global json_top
    global json_ut
   
    msg_decode = message.payload.decode('utf-8')
    json_msg = json.loads(msg_decode)

    #Tri des messages en fonction du topic de provenance
    if(message.topic == topic_pos):
        json_init = json_msg
    elif(message.topic == topic_cars_info):
        json_cars = json_msg
    elif(message.topic == topic_traficlight_info):
        json_traficlight = json_msg
    elif(message.topic == topic_top):
        json_top = json_msg
    elif(message.topic == topic_ut):
        json_ut = json_msg
  
    
if(__name__ == "__main__"):

    #Lecture du fichier de config
    config = cfg.ConfigParser()
    config.read("config.ini")

    #Info de connexion a la file mqtt
    host = str(config['mqtt']['host'])
    port = int(config['mqtt']['port'])

    #Initalisation des topic
    topic_pos = str(config['topic']['positions']).strip('"')
    topic_cars_info = str(config['topic']['cars_info']).strip('"')
    topic_traficlight_info = str(config['topic']['traficlight_info']).strip('"')
    topic_top = str(config['topic']['top']).strip('"')
    topic_ut = str(config['topic']['ut']).strip('"')
    topic_resp = str(config['topic']['resp']).strip('"')

    #initialisation des clients de la file mqtt
    handler_positions = mqtt_handler(host, port, id, topic_pos, on_message)
    mqtt_pos = handler_positions.get_client()
    mqtt_pos.on_message = on_message
    mqtt_pos.subscribe(topic_pos)
    mqtt_pos.loop_start()

    handler_cars = mqtt_handler(host, port, id, topic_cars_info, on_message)
    mqtt_cars = handler_cars.get_client()
    mqtt_cars.on_message = on_message
    mqtt_cars.subscribe(topic_cars_info)
    mqtt_cars.loop_start()

    if(test == True):
        handler_tester = mqtt_handler(host, port, id, topic_ut, on_message)
        mqtt_ut = handler_tester.get_client()
        mqtt_ut.on_message = on_message
        mqtt_ut.subscribe(topic_ut)
        mqtt_ut.loop_start()

        handler_resp = mqtt_handler(host, port, id, topic_resp, on_message)
        mqtt_resp = handler_resp.get_client()


    handler_traficlight = mqtt_handler(host, port, id, topic_traficlight_info, on_message)
    mqtt_traficlight = handler_traficlight.get_client()
    mqtt_traficlight.on_message = on_message
    mqtt_traficlight.subscribe(topic_traficlight_info)
    mqtt_traficlight.loop_start()

    handler_top = mqtt_handler(host, port, id, topic_top, on_message)
    mqtt_top = handler_top.get_client()
    mqtt_top.on_message = on_message
    mqtt_top.subscribe(topic_top)
    mqtt_top.loop_start()

    #initialisation de la carte
    carte = map("carte.ini")

    #Recuperation de la carte pour la phase de test local
    with open("trip.json", 'r') as file:
        file_json = json.load(file)

    #Initialisation de la voiture
    vehicule = car.car(carte, json.dumps(file_json))
    #print(vehicule.to_json())

    #Initialisation de la vue
    view_map = view(carte, vehicule)

    #while(json_top == None):
     #   pass
    
    horloge = 0

    #Demmarage de la voiture
    while(True):
        time.sleep(1)
        json_pos = vehicule.move_to_next_pos()
        print(json_pos)
        
        mqtt_cars.publish(topic_cars_info, json_pos)
        if(json_traficlight != None):
            carte.update_traficlight(json_traficlight)
        if(json_init != None):
            carte.update_dict_car_info(json_init)
            carte.init_cars(json_init)

        #UpperTester
        horloge_test = horloge
        pos_test = vehicule.get_pos()
        if(test == True):
            if(json_ut != None):
                if(json_ut["id"] == vehicule.get_id()):
                    json_ut_rep = {'id' : vehicule.get_id(),
                                   'temps' : horloge_test,
                                   'position' : str(pos_test[0])+","+str(pos_test[1])}
                    mqtt_resp.publish(topic_resp, json.dumps(json_ut_rep))
        
        #Mise à jour de la carte interactive
        view_map.update_view(vehicule, carte)