import configparser as cfg
import json
from section import *
from traficlight import *

class map:
    #La construction initial de la map se fait a partir du fichier carte.ini
    def __init__(self, filename):
        #Initalisation du configParser
        config = cfg.ConfigParser()
        config.read(filename)

        #Initialisation des champs
        self.nb_section = int(config['generalInfo']['nbHops'])
        self.nb_traficlight = int(config['generalInfo']['nbTrafficLight'])
        self.size = (100,100)

        #Initialisation de la liste des tronçons de la carte
        self.list_section = []
        self.__init_list_section(config)

        #Initialisation de la liste des feux sur la carte
        self.list_traficlight = []
        self.__init_list_traficlight(config)

        #Initialisation du dict d'info des vehicule sur la carte
        self.dict_car_info = {}
      

    #Lecture de tout les tronçons de la carte dans le fichier de config, chargement des tronçons dans une liste
    def __init_list_section(self, config):
        for i in range(1, self.nb_section+1):
            list_pos_section = config['trip'][str(i)].split(',')
            #[0], [1] -> Coor 1
            #[2], [3] -> Coor 2
            self.list_section.append(section(posx1=list_pos_section[0],
                                             posy1=list_pos_section[1], 
                                             posx2=list_pos_section[2], 
                                             posy2=list_pos_section[3], 
                                             id=i))


    #Lecture de tout les feux dans le fichier de config, chargement des feux dans une liste
    def __init_list_traficlight(self, config):
        for i in range(1, self.nb_traficlight+1):
            list_pos_traficlight = config['trafficLight'][str(i)].split(',')
            for j in range(0,4):
                self.list_traficlight.append(traficlight(posx=list_pos_traficlight[0], 
                                                        posy=list_pos_traficlight[1],
                                                        id=i,
                                                        dir=j))

                                                 

    #Met a jour le dictionnaire des informations sur les autres vehicule sur la carte
    def update_dict_car_info(self, msg):
        print("update car info:")
        print(msg)

    #Getter
    def get_nb_section(self):
        return self.nb_section
    
    def get_nb_traficlight(self):
        return self.nb_traficlight
    
    def get_full_list_section(self):
        return self.list_section
    
    def get_full_list_traficlight(self):
        return self.list_traficlight
    
    #Retourne un tronçon grace a son id si il existe, -1 sinon
    def get_section_by_id(self, id):
        if(1 <= id <= 136):
            return self.list_section[id]
        else:
            return -1
    
    #Retourne l'id de la section si les coordonnées correspondent a une section, -1 sinon.
    def get_section_by_coor(self, x, y):
        found = 0
        for section in self.list_section:
            if(in_range(x, section.get_pos_ext1()[0], section.get_pos_ext2()[0])) and (in_range(y, section.get_pos_ext1()[1], section.get_pos_ext2()[1])):
                found = 1
                return section
        if(found == 0):
            return -1

    def get_traficlight_by_id(self, id):
        return self.list_traficlight[id]
    
    #Prend un objet section en parametre et retourne l'ensemble des feu se toruvant sur cette section
    def get_traficlight_by_section(self, section):
        list_traficlight_in_section = []
        section_ext1 = section.get_pos_ext1()
        section_ext2 = section.get_pos_ext2()
        
        for traficlight in self.list_traficlight:
            posx, posy = traficlight.get_pos()
            if(posx == section_ext1[0] and posy == section_ext1[1]) or (posx == section_ext2[0] and posy == section_ext2[1]):
                list_traficlight_in_section.append(traficlight)
        return list_traficlight_in_section
    
    #Retourne ue liste de positions correspondant aux positions des autres vehicule sur la carte
    def get_dict_pos_car(self):
        return self.dict_car_info
    
    def update_traficlight(self, new_light):
        print(new_light)
        for light_id, light_state_str in new_light.items():
            # Récupération de l'id du feu et son état depuis le message JSON
            light_state_list = list(map(int, light_state_str.split(',')))  # Convertir l'état en liste d'entiers
            print(light_id)
            print(light_state_list)

            # Trouver le feu correspondant dans la liste des feux
            found_light = False
            for light in self.list_traficlight:
                if light.get_id() == light_id:
                    found_light = True
                    # Mise à jour de l'état du feu
                    light.update_state(light_state_list)
                    break 

    def init_cars(self, cars_list):
        self.dict_car_info = cars_list
       

#Retourne vrai si la valeur se trouve entre les deux intervalle peu importe le sens [0,10] ou [10,0]
def in_range(value, intv1, intv2):
    if(intv1 <= value <= intv2) or (intv2 <= value <= intv1):
        return True
    else:
        return False