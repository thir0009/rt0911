On a un objet voiture caractérisé par : 
- posx, posy -> Les positions actuelles de la voiture
- init_posx, init_posy -> Les positions initial de la voiture sur la carte
- vitesse -> le nombre d'unité max que la voiture peu parcourir en une seconde
- id -> l'identifiant de la voiture
- liste_tronçons -> la liste des tronçons que la voiture doit parcourir pour arriver au point final.
- etat -> depart, en route, arrivé

La voiture possède une fonction qui calcul a l'avance se prochaine position avant de se deplacer
move(self, carte) : cette fonction calcul le prochain point sur lequel la voiture se trouvera au prochain deplacement en prenant en compte la vitesse de la voiture, si une autre voiture se trouve sur cette emplacement grace au dictionnaire de position des autres vehicule et si il n'y a pas de feu rouge.
Elle prend également en compte le passage a un nouveau tronçons pour savoir si la voiture doit tourner ou pas.
Si le point suivant calculé est deja occupé, alors on passe au point suivant-1, etc tant que le resultat est >= a la position actuel.
Tant que la liste_tronçon est vide, la voiture ne peut pas bouger.

La voiture possede une fonction to_Json() utilisé pour publier la position et l'id de celle-ci dans la file.

La voiture possede une fonction get_trip() qui met a jour la liste_tronçons à partir d'un fichier json reçon depuis la file mqtt d'initialisation.

Chaque deplacement de la voiture conduit a une publication dans la file, meme si la voiture n'a pas bougé.

------------------------------------------------------------------------------------------------------------------------------------------------------------
Un objet feu caracterisé par : 
- Une position X,Y
- L'id du tronçon sur lequel il se trouve
- l'etat du feu (rouge ou vert)
On a un feu a chaque croisement, mais chaque feu fait face a un tronçons donc on considere ce feu en 4 feu distinct et rattaché a un tronçon grace a son id.

Le mise a jour des feux se fait grace a une file mqtt, l'infos est contenu dans le meme message que les positions des vehicule.
La carte gere le declenchement de la mise a jour du feu grace a l'id.

------------------------------------------------------------------------------------------------------------------------------------------------------------
Un objet tronçons carcatérisé par : 
- X1,Y1 la premiere extrémité du tronçons
- X2, Y2 l'autre extrémité du tronçons
- Un id de tronçons
- une liste de feu

------------------------------------------------------------------------------------------------------------------------------------------------------------
Un objet carte caractérisé par : 
- taille X, taille Y -> Fixé a 100/100
- le nb de tronçons
- le nb de feu
- Une liste de tronçons
- un dictionnaire des positions des autres voitures

On créée l'objet carte a partir d'un fichier ini, celui-ci se compose de la liste des tronçons et de leur id.
ainsi que de la liste des feu et leur position.
Pour l'assignation des feu, il est necessaire de quadruplé ceux-ci afin de placer un feu dans chaque tronçons adjacent au point.

On envoie a chaque seconde l'etat de l'objet voiture dans une file mqtt et on recupere l'etat des autre voiture sur la carte depuis une autre file mqtt, la reception d'un message met a jour un dictionnaire de dictionnaire tel que : 
{
	id : {x,y},
	id2 : {x2,y2}
}

Il est necessaire de s'abonner a 2 file mqtt, la premiere qui donne a la voiture son etat initial,
La secondes donne toute les sec l'etat des autres vehicules et des feux sur la carte.

Un objet de gestion mqtt permettant de s'abonner a ses différentes files mqtt et de publier dans la 3eme file mqtt.
------------------------------------------------------------------------------------------------------------------------------------------------------------
Recuperation des infos des autres vehicule en temps réel depuis une file mqtt : 
le thread consomme une message de la file mqtt, ce message est un string json de la forme : 
 {
 "id": "032",
 "Pos": "20,30",
 "slot": "22"
 }

------------------------------------------------------------------------------------------------------------------------------------------------------------
Pour deplacer le vehicule apres avoir determiné sa direction, je propose de faire les deplacement un par un et non en faisait un deplacement de taille pace.
Ainsi, au lieu de corriger la trajectoire on la calcule en fonction de la position n+1.
Pour cela, en fonction de la direction on calcule la position n+1.
Tant qu'on a pas fait pace pas on verifie
Si la position n+1 est libre et sur le tronçon actuelle alors on avance.
Si la position n+1 n'est pas libre, on avance pas et on retourne position actuelle comme une nouvelle position.
Si la position n+1 n'est pas sur le tronçon, on doit recuperer le tronçon suivant et se placer sur celui-ci, ce qui implique un changement de direction.
On recuperer alors la position ext2 du tronçon suivant dans la liste trip, on calcul la nouvelle direction et on essaye d'avancer.


On peut envisager de deporter le calcul de direction en fonction d'une extremité et d'une position dans une fonction externe.
movedirection(ext, x, y) -> int
-------------------------------------------------------------------------------------------------------------------------------------

On a un probleme de mise a jour des extremité apres un virage, on garde toujours les meme extremités
Il est necessaire de faire la mise a jour de ext1 et ext2.
Pour cela on devrait ecrire une fonction get_next_section(section) qui retourne la section qui suit la section courrante.

-------------------------------------------------------------------------------------------------------------------------------------------

Ce que l'on recupere depuis la file positions : 
