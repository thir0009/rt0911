import tkinter as tk
from map import *
from etat import Etat


class view:

    def __init__(self, map, car):

        #Initialisation de la fenetre
        self.frame = tk.Tk()
        self.frame.title("Carte")
        self.width = 400
        self.height = 400
        
        #Creation du canvas
        self.canvas = tk.Canvas(self.frame, width=self.width, height=self.height)
        self.canvas.pack()

        #Recuperation des infos de la carte
        self.list_section = map.get_full_list_section()
        self.list_traficlight = map.get_full_list_traficlight()

        self.update_view(car, map)
        

    #Fonction affichant les routes
    def draw_section(self, ext1, ext2):
        self.canvas.create_line(ext1[0]*4, self.height- ext1[1]*4, ext2[0]*4, self.height - ext2[1]*4, fill='black')
    
    #Fonction affichant les feux
    def draw_traficlight(self, pos, p_etat):

        #On determine la couleur (grey si le feu n'a aucun etta)
        couleur = "grey"
        if(p_etat == Etat.GREEN):
            couleur = "green"
        elif(p_etat == Etat.RED):
            couleur = "red"
        
        #Recuperation des position et agrandissement pour la vue
        posx = pos[0]*4
        posy = self.height - pos[1]*4

        

        #Affichage du cercle
        self.canvas.create_oval(posx-5, posy-5, posx+5, posy+5, fill=couleur, outline="black")
    
    #Fonction affichant la voiture
    def draw_car(self, car):
        posx, posy = car.get_pos()
        posx = posx*4
        posy = self.height - posy*4

        #Recuperation de la direction 
        direction = car.get_dir()
        liste_points = [posx, posy, posx + 5, posy + 10, posx - 5, posy + 10]
        
        if(direction == 0):
            liste_points = [posx, posy, posx - 10, posy + 5, posx - 10, posy - 5]
        elif(direction == 2):
            liste_points = [posx, posy, posx + 10, posy + 5, posx + 10, posy - 5]
        elif(direction == 1):
            liste_points = [posx, posy, posx + 5, posy + 10, posx - 5, posy + 10]
        elif(direction == 3):
            liste_points = [posx, posy, posx + 5, posy - 10, posx - 5, posy - 10]
        self.canvas.create_polygon(liste_points, fill="blue")

    def draw_friends(self, friend_list):
        for friend in friend_list:
            if(int(friend) != 6):
           
                posx = int(friend_list[friend]['x'])
                posy = int(friend_list[friend]['y'])
                posx = posx*4
                posy = self.height - posy*4
            
                #Recuperation de la direction 
                direction =  int(friend_list[friend]['dir'])
                liste_points = [posx, posy, posx + 5, posy + 10, posx - 5, posy + 10]
                
                if(direction == 0):
                    liste_points = [posx, posy, posx - 10, posy + 5, posx - 10, posy - 5]
                elif(direction == 2):
                    liste_points = [posx, posy, posx + 10, posy + 5, posx + 10, posy - 5]
                elif(direction == 1):
                    liste_points = [posx, posy, posx + 5, posy + 10, posx - 5, posy + 10]
                elif(direction == 3):
                    liste_points = [posx, posy, posx + 5, posy - 10, posx - 5, posy - 10]
                self.canvas.create_polygon(liste_points, fill="red")  

    #Fonction mettant a jour la vuer
    def update_view(self, car, map):
        self.canvas.delete("all")
        #Recuperation des infos de la carte
        self.list_section = map.get_full_list_section()
        self.list_traficlight = map.get_full_list_traficlight()

        #Affichage des section
        for section in self.list_section:
            self.draw_section(section.get_pos_ext1(), section.get_pos_ext2())
        
        #Affichage des feux 
        for traficlight in self.list_traficlight:
            self.draw_traficlight(traficlight.get_pos(), traficlight.get_etat())
        
        #Affichage de la voiture
        self.draw_car(car)
        self.draw_friends(map.get_dict_pos_car())
        self.frame.update()
      
    
    def start(self):
        self.frame.mainloop()