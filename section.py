from traficlight import *

class section:
    def __init__(self, posx1, posy1, posx2, posy2, id):
        self.ext1 = int(posx1), int(posy1)
        self.ext2 = int(posx2), int(posy2)
        self.id = id
    
    #Getter
    #Retourne un tuple des 2 extremité d'un tronçon
    def get_pos_ext1(self):
        return self.ext1
    
    def get_pos_ext2(self):
        return self.ext2
    
    def get_id(self):
        return self.id
    
    def print_section(self):
        print("Ext1 : ", self.ext1, "Ext2 : ", self.ext2, "id : ", self.id)