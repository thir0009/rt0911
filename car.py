from map import *
from section import *
from etat import Etat
import json, time

class car:
    def __init__(self, map, car_file):

        self.car_file = car_file    #On considere un fichier json reçu depuis un broker mqtt
        self.map = map
        self.posx = -1
        self.posy = -1
        self.id = -1
        self.trip = []            #On considere trip comme une liste de tronçons par lesquelles la voiture doit passer, ces tronçons sont trier par ordre de passage.
        self.pace = -1
        self.direction = 0
        self.type = 1
        self.init_car_from_msg()

#Getter
    def get_map(self):
        return self.map
    
    def get_pos(self):
        return self.posx, self.posy
    
    def get_id(self):
        return self.id
    
    def get_trip(self):
        return self.trip
    
    def get_pace(self):
        return self.pace
    
    def get_dir(self):
        return self.direction
    
#Fonction calculant et deplaçant la voiture a sa prochaine positon, retourne la nouvelle representation json de la voiture
    def move_to_next_pos(self):

        #Recherche du positionnement du tronçon sur le trajet de la voiture
        pos_trip = self.get_section_in_trip(self.posx, self.posy)

        #Recuperation de la section courrante
        curr_section = self.trip[pos_trip]
        
        #extremité de fin du tronçon actuelle
        if(pos_trip != -1):
            
            #Recuperation des positions des autres vehicule
            info = self.map.get_dict_pos_car()

            #Initialisation des step
            step = 0
            
            #Tant qu'on a pas fait le deplacement complet, on continue
            while(step < self.pace):
                ext2 = self.trip[pos_trip].get_pos_ext2()

                #On recupere le debut du tronçon suivant pour connaitre le sens
                #Si ce tronçon n'existe pas, on est sur le tronçon d'arriver, on se dirige alors vers ext2
                if(pos_trip+1 < len(self.trip)):
                    next_section = self.trip[pos_trip+1]
                    ext_dest1 = next_section.get_pos_ext1()
                    ext_dest2 = next_section.get_pos_ext2()
                else:
                    ext_dest1 = ext2
            
                #On souhaite toujours se diriger vers ext_dest1
                self.direction = self.get_direction(self.posx, self.posy, ext_dest1)

                #On recupere la liste des feux sur la section
                list_traficlight = self.map.get_traficlight_by_section(curr_section)

                if(self.direction == 0):
                    #Droite
                    next_posx = self.posx+1
                    next_posy = self.posy

                    #On verifie qu'on est toujours sur le tronçon courant, si oui on avance. (incrementation de step)
                    if(self.get_section_in_trip(next_posx, next_posy) == pos_trip):
                        if(self.is_free(next_posx, next_posy) and self.check_traficlight(next_posx, next_posy, curr_section, list_traficlight)):
                            self.posx = next_posx
                            self.posy = next_posy
                        step = step+1
                    #On verifie si on est arrivé a la premiere extremité de la section suivant.
                    #La section suivante deviens alors la section actuelle, et on recupere la section suivante. (incrementation de pos_trip)
                    #Pas de mouvement dans ce cas, cela interviendra au prochain tour de boucle
                    elif(next_posx == ext_dest1[0] and next_posy == ext_dest1[1]):
                        if(pos_trip+1 < len(self.trip)):
                            curr_section = next_section
                            pos_trip = pos_trip+1

                elif(self.direction == 2):
                    #Gauche
                    next_posx = self.posx-1
                    next_posy = self.posy

                    #On verifie qu'on est toujours sur le tronçon courant, si oui on avance. (incrementation de step)
                    if(self.get_section_in_trip(next_posx, next_posy) == pos_trip):
                        if(self.is_free(next_posx, next_posy) and self.check_traficlight(next_posx, next_posy, curr_section, list_traficlight)):
                            self.posx = next_posx
                            self.posy = next_posy
                        step = step+1
                    elif(next_posx == ext_dest1[0] and next_posy == ext_dest1[1]):
                        if(pos_trip+1 < len(self.trip)):
                            curr_section = next_section
                            pos_trip = pos_trip+1

                elif(self.direction == 1):
                    #Haut
                    next_posx = self.posx
                    next_posy = self.posy+1
                    #On verifie qu'on est toujours sur le tronçon courant, si oui on avance. (incrementation de step)
                    if(self.get_section_in_trip(next_posx, next_posy) == pos_trip):
                        if(self.is_free(next_posx, next_posy) and self.check_traficlight(next_posx, next_posy, curr_section, list_traficlight)):
                            self.posx = next_posx
                            self.posy = next_posy
                        step = step+1
                    elif(next_posx == ext_dest1[0] and next_posy == ext_dest1[1]):
                        if(pos_trip+1 < len(self.trip)):
                            curr_section = next_section
                            pos_trip = pos_trip+1

                elif(self.direction == 3):
                    #Bas
                    next_posx = self.posx
                    next_posy = self.posy-1
                    #On verifie qu'on est toujours sur le tronçon courant, si oui on avance. (incrementation de step)
                    if(self.get_section_in_trip(next_posx, next_posy) == pos_trip):
                        if(self.is_free(next_posx, next_posy) and self.check_traficlight(next_posx, next_posy, curr_section, list_traficlight)):
                            self.posx = next_posx
                            self.posy = next_posy
                        step = step+1
                    elif(next_posx == ext_dest1[0] and next_posy == ext_dest1[1]):
                        if(pos_trip+1 < len(self.trip)):
                            curr_section = next_section
                            pos_trip = pos_trip+1

                elif(self.direction == -1):
                    if(pos_trip+1 < len(self.trip)):
                        curr_section = next_section
                        pos_trip = pos_trip+1
            return self.to_json()

#Retourne l'etat de la voiture au format json pour la file mqtt
    def to_json(self):
        dict_car = {
            "id":str(self.id),
            "vtype":str(self.type),
            "x":str(self.posx),
            "y":str(self.posy),
            "dir":str(self.direction),
            "speed":str(self.pace)  
        }

        return json.dumps(dict_car)

#initialisation de la voiture avec les infos du message
    def init_car_from_msg(self):
        json_msg = json.loads(self.car_file)
        self.id = str(json_msg["id"])
        self.pace = int(json_msg["pace"])
        pos = json_msg["pos_init"].split(',')
        self.posx = int(pos[0])
        self.posy = int(pos[1])
        list_section_trip = []
        i = 1

        for elem in json_msg["trip"]:
            list_section_trip = elem.split(',')
            self.trip.append(section(list_section_trip[0],
                                     list_section_trip[1],
                                     list_section_trip[2],
                                     list_section_trip[3],
                                     id=i))
            i += 1

#Verifie si une position sur la carte est libre en considerant d'office qu'elle est sur un tronçon
    def is_free(self, x, y):
        '''for k,v in self.map.get_dict_pos_car():
            if(int(v["posx"]) == int(x) and int(v["posy"]) == int(y)):
                return False'''
        return True
    
    '''
    Stationnaire = -1
    Droite = 0
    Gauche = 2
    Haut = 1
    Bas = 3
    '''
#Calcul de la direction du vehicule en fonction de sa position et de sa destination sur le tronçon
    def get_direction(self, x, y, ext):
        direction = -1
        if(ext[0] > x):
            direction = 0
        elif(ext[0] < x):
            direction = 2
        elif(ext[1] > y):
            direction = 1
        elif(ext[1] < y):
            direction = 3
        return direction

#Fonction retournant la section dans laquel se trouve la voitue
    def get_section_in_trip(self, posx, posy):
        found = 0
        for i in range(0, len(self.trip)):
            if(in_range(posx, self.trip[i].get_pos_ext1()[0], self.trip[i].get_pos_ext2()[0]) and in_range(posy, self.trip[i].get_pos_ext1()[1], self.trip[i].get_pos_ext2()[1])):
                found = 1
                return i

        if(found == 0):
            return -1
        
#Fonction permettant une gestion des feux
#Retourne True si pas de feux sur la prochaine position ou si il y a un feu vert sur la prochaine position
#Retourne False si il y a un feu et qu'il est rouge
    def check_traficlight(self, posx, posy, section, list_traficlight):
        found = 0
        for traficlight in list_traficlight:

            #On verifie si il y a un feu sur la position
            if(traficlight.get_pos()[0] == posx and traficlight.get_pos()[1] == posy):
                found = 1
                #On verifie si le feu sur cette position est vert
                if(traficlight.get_etat() == Etat.GREEN):
                    #Tout est ok on peut rouler
                    print("Vert")
                    return True
                elif(traficlight.get_etat() == Etat.RED):
                    #Feu rouge, on ne roule pas
                    return False
        #On a pas toruver de feu sur la position, on peut avancer
        if(found == 0):
            return True


#Retourne vrai si la valeur se trouve entre les deux intervalle peu importe le sens [0,10] ou [10,0]
def in_range(value, intv1, intv2):
    if(intv1 <= value <= intv2) or (intv2 <= value <= intv1):
        return True
    else:
        return False
